package com.qniyaz.tictactoemvvm.viewmodel;

public interface ViewModel {

    void onCreate();
    void onPause();
    void onResume();
    void onDestroy();

}
