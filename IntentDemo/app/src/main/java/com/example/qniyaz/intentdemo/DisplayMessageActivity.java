package com.example.qniyaz.intentdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        Intent intent = getIntent();
        TextView textView = (TextView) findViewById(R.id.textView) ;
        String str = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        textView.setText(str);
    }
}
