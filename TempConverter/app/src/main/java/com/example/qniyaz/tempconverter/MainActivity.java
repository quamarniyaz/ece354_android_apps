package com.example.qniyaz.tempconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {

    public double convertedTemperature = 0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void convertTemperature(View view){
        EditText tempText = (EditText) findViewById(R.id.editText1) ;
        EditText convertedText = (EditText) findViewById(R.id.editText2) ;
        if(tempText.length() > 0) {
            float temperature = Float.valueOf(tempText.getText().toString()).floatValue();
            RadioButton rb = (RadioButton) findViewById(R.id.radioButton);
            if (rb.isChecked()) {
                convertedTemperature = (temperature - 32.0) * 5 / 9;
                convertedText.setText(Double.toString(convertedTemperature));
            } else {
                convertedTemperature = (temperature - 0.0) * 9 / 5 + 32.0;
                convertedText.setText(Double.toString(convertedTemperature));
            }
        }

    }
}
