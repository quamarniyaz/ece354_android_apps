package com.qniyaz.tictactoemvp.presenter;

public interface Presenter {

    void onCreate();
    void onPause();
    void onResume();
    void onDestroy();

}
